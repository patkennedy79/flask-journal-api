import datetime


# ----------------
# Helper Functions
# ----------------

def check_datetime_values(value1: datetime, value2: datetime):
    """Compares two datetime objects based on date, hour, minutes, and seconds."""
    assert value1.date() == value2.date()
    assert value1.hour == value2.hour
    assert value1.minute == value2.minute
    assert value1.second == value2.second
