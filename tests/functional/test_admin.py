"""
This file (test_admin.py) contains the functional tests for the 'admin' blueprint.
"""
from project.models import Entry, User


# --------------
# Login / Logout
# --------------

def test_admin_login_get_page(test_client):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/admin/login' page is requested (GET)
    THEN check the response is valid
    """
    response = test_client.get('/admin/login')
    assert response.status_code == 200
    assert b'Login' in response.data
    assert b'Email' in response.data
    assert b'Password' in response.data
    assert b'Login' in response.data


def test_admin_login_valid_login_and_logout(test_client, admin_user):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/admin/login' page is posted to (POST) with valid credentials for an admin user
    THEN check the response is valid
    """
    response = test_client.post('/admin/login',
                                data={'email': 'patrick_admin@email.com',
                                      'password': 'FlaskIsTheBest987'},
                                follow_redirects=True)
    assert response.status_code == 200
    assert b'Flask Journal App' in response.data
    assert b'Please log in to access this page.' not in response.data

    """
    GIVEN a Flask application
    WHEN the '/admin/logout' page is requested (GET) for a logged in user
    THEN check the response is valid
    """
    response = test_client.get('/admin/logout', follow_redirects=True)
    assert response.status_code == 200
    assert b'Goodbye!' in response.data
    assert b'Flask Journal App' in response.data
    assert b'Please log in to access this page.' not in response.data


def test_admin_login_incorrect_password(test_client, admin_user):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/admin/login' page is posted to (POST) with invalid credentials (incorrect password)
    THEN check an error message is returned to the user
    """
    response = test_client.post('/admin/login',
                                data={'email': 'patrick_admin@email.com',
                                      'password': 'FlaskIsNotAwesome'},  # Incorrect!
                                follow_redirects=True)
    assert response.status_code == 200
    assert b'ERROR! Incorrect login credentials.' in response.data
    assert b'Flask Journal App' in response.data


def test_admin_login_non_admin_user(test_client, default_user):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/admin/login' page is posted to (POST) with valid credentials for a non-admin user
    THEN check an error message is returned to the user
    """
    response = test_client.post('/admin/login',
                                data={'email': 'pkennedy@hey.com',
                                      'password': 'FlaskIsTheBest'},
                                follow_redirects=True)
    assert response.status_code == 200
    assert b'ERROR! Incorrect login credentials.' in response.data
    assert b'Flask Journal App' in response.data


def test_admin_login_invalid_user(test_client):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/admin/login' page is posted to (POST) with invalid credentials (non-registered user)
    THEN check an error message is returned to the user
    """
    response = test_client.post('/admin/login',
                                data={'email': 'patrick@gmail.com',  # Non-registered user
                                      'password': 'FlaskIsTheBest'},
                                follow_redirects=True)
    assert response.status_code == 200
    assert b'ERROR! Incorrect login credentials.' in response.data
    assert b'Flask Journal App' in response.data


def test_admin_logout_invalid(test_client):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/admin/logout' page is posted to (POST)
    THEN check that a 405 error is returned
    """
    response = test_client.post('/admin/logout', follow_redirects=True)
    assert response.status_code == 405


# -----------------
# Invalid Scenarios
# -----------------

def test_admin_no_logged_in_user(test_client):
    """
    GIVEN a Flask application configured for testing
    WHEN specific URLs only accessible by an Admin user are requested (GET) when no one is logged in
    THEN check an error message is returned for each URL
    """
    urls = ['/admin/logout',
            '/admin/users/',
            '/admin/users/1/confirm_email',
            '/admin/users/1/unconfirm_email',
            '/admin/users/1/change_password',
            '/admin/users/3/revoke_token',
            '/admin/users/2/delete_user']

    for url in urls:
        response = test_client.get(url, follow_redirects=True)
        assert response.status_code == 200
        assert b'Flask Journal App' in response.data
        assert b'Please log in to access this page.' in response.data


def test_admin_invalid_index(test_client, default_user, admin_user_login):
    """
    GIVEN a Flask application configured for testing
    WHEN specific URLs only accessible by an Admin user are requested (GET) with an invalid index
    THEN check that a 404 (Not Found) response is returned
    """
    urls = ['/admin/users/123/confirm_email',
            '/admin/users/234/unconfirm_email',
            '/admin/users/345/change_password',
            '/admin/users/456/revoke_token',
            '/admin/users/567/delete_user']

    for url in urls:
        response = test_client.get(url, follow_redirects=True)
        print(url)
        assert response.status_code == 404


# ----------
# List Users
# ----------

def test_admin_list_users_valid(test_client, default_user, admin_user_login):
    """
    GIVEN a Flask application configured for testing with the admin user logged in
          and the default set of users in the database
    WHEN the '/admin/users/' page is requested (GET)
    THEN check the response is valid and all expected users are listed
    """
    expected_users = [b'patrick_admin@email.com',  # Admin
                      b'pkennedy@hey.com',
                      b'patkennedy79@gmail.com']

    response = test_client.get('/admin/users/', follow_redirects=True)
    assert response.status_code == 200
    assert b'Flask Journal App' in response.data
    assert b'List of Users' in response.data
    for expected_user in expected_users:
        assert expected_user in response.data
    assert b'ID' in response.data
    assert b'Email' in response.data
    assert b'Registration Date' in response.data
    assert b'Email Confirmation Date' in response.data
    assert b'User Type' in response.data
    assert b'# of Journal Entries' in response.data


# -------------
# Confirm Email
# -------------

def test_admin_confirm_email_valid(test_client, default_user, admin_user_login):
    """
    GIVEN a Flask application configured for testing with the admin user logged in
          and the default set of users in the database
    WHEN the '/admin/users/1/confirm_email' page is requested (GET)
    THEN check the response is valid and user #1's email address is confirmed
    """
    # Check that user #1's email address is NOT confirmed
    user = User.query.filter_by(id=1).first()
    assert not user.email_confirmed

    response = test_client.get('/admin/users/1/confirm_email', follow_redirects=True)
    assert response.status_code == 200
    assert b'Flask Journal App' in response.data
    assert b'List of Users' in response.data
    assert b'Email address confirmed for patrick_admin@email.com' in response.data

    # Check that user #1's email address is confirmed
    user = User.query.filter_by(id=1).first()
    assert user.email_confirmed


# ----------------
# Un-confirm Email
# ----------------

def test_admin_unconfirm_email_valid(test_client, default_user, admin_user_login):
    """
    GIVEN a Flask application configured for testing with the admin user logged in
          and the default set of users in the database
    WHEN the '/admin/users/1/unconfirm_email' page is requested (GET)
    THEN check the response is valid and user #1's email address is no longer confirmed
    """
    # Check that user #1's email address is confirmed
    user = User.query.filter_by(id=1).first()
    assert user.email_confirmed

    response = test_client.get('/admin/users/1/unconfirm_email', follow_redirects=True)
    assert response.status_code == 200
    assert b'Flask Journal App' in response.data
    assert b'List of Users' in response.data
    assert b'Email address confirmation revoked for patrick_admin@email.com' in response.data

    # Check that user #1's email address is NOT confirmed
    user = User.query.filter_by(id=1).first()
    assert not user.email_confirmed


# ---------------
# Change Password
# ---------------

def test_admin_change_password_get_valid(test_client, default_user, admin_user_login):
    """
    GIVEN a Flask application configured for testing with the admin user logged in
          and the default set of users in the database
    WHEN the '/admin/users/2/change_password' page is requested (GET)
    THEN check the response is valid
    """
    response = test_client.get('/admin/users/2/change_password', follow_redirects=True)
    assert response.status_code == 200
    assert b'Flask Journal App' in response.data
    assert b'Password Reset' in response.data
    assert b'New Password' in response.data
    assert b'Submit' in response.data


def test_admin_change_password_post_valid(test_client, default_user, admin_user_login):
    """
    GIVEN a Flask application configured for testing with the admin user logged in
          and the default set of users in the database
    WHEN the '/admin/users/2/change_password' page is posted to (POST) with valid data
    THEN check the response is valid
    """
    response = test_client.post('/admin/users/2/change_password',
                                data={'password': 'FlaskIsTheBest'},
                                follow_redirects=True)
    assert response.status_code == 200
    assert b'Flask Journal App' in response.data
    assert b'Password changed for pkennedy@hey.com' in response.data


def test_admin_change_password_post_invalid(test_client, default_user, admin_user_login):
    """
    GIVEN a Flask application configured for testing with the admin user logged in
          and the default set of users in the database
    WHEN the '/admin/users/234/change_password' page is posted to (POST) with valid data
    THEN check that a 404 (Not Found) response is returned
    """
    response = test_client.post('/admin/users/234/change_password',
                                data={'password': 'FlaskIsTheBest'},
                                follow_redirects=True)
    assert response.status_code == 404


# ---------------------------
# Revoke Authentication Token
# ---------------------------

def test_admin_revoke_token_valid(test_client, default_user, admin_user_login):
    """
    GIVEN a Flask application configured for testing with the admin user logged in
          and the default set of users in the database
    WHEN the '/admin/users/3/revoke_token' page is requested (GET)
    THEN check the response is valid
    """
    response = test_client.get('/admin/users/3/revoke_token', follow_redirects=True)
    assert response.status_code == 200
    assert b'Flask Journal App' in response.data
    assert b'List of Users' in response.data
    assert b'Authentication token revoked for patkennedy79@gmail.com' in response.data


# -----------
# Delete User
# -----------

def test_admin_delete_user_valid(test_client, default_user, admin_user_login, default_journal):
    """
    GIVEN a Flask application configured for testing with the admin user logged in
          and the default set of users in the database
    WHEN the '/admin/users/2/delete_user' page is requested (GET)
    THEN check the response is valid
    """
    # Check that user #2 exists in the database
    user = User.query.filter_by(id=2).first()
    assert user.email == 'pkennedy@hey.com'

    # Check that there are 3 journal entries associated with user #2
    entries = Entry.query.filter_by(user_id=2).all()
    assert len(entries) == 3

    response = test_client.get('/admin/users/2/delete_user', follow_redirects=True)
    assert response.status_code == 200
    assert b'Flask Journal App' in response.data
    assert b'List of Users' in response.data
    assert b'User (pkennedy@hey.com) and their associated journal entries were deleted!' in response.data

    # Check that user #2 is no longer in the database
    user = User.query.filter_by(id=2).first()
    assert user is None

    # Check that there are no journal entries associated with user #2
    entries = Entry.query.filter_by(user_id=2).all()
    assert len(entries) == 0


def test_admin_delete_user_invalid_delete_admin(test_client, default_user, admin_user_login, default_journal):
    """
    GIVEN a Flask application configured for testing with the admin user logged in
          and the default set of users in the database
    WHEN the '/admin/users/1/delete_user' page is requested (GET)
    THEN check that the admin user is not deleted
    """
    response = test_client.get('/admin/users/1/delete_user', follow_redirects=True)
    assert response.status_code == 200
    assert b'Flask Journal App' in response.data
    assert b'List of Users' in response.data
    assert b'Cannot delete Administrator (patrick_admin@email.com)!' in response.data

    # Check that user #1 is still in the database
    user = User.query.filter_by(id=1).first()
    assert user.email == 'patrick_admin@email.com'
