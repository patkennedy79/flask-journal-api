"""
This file (test_users_api.py) contains the functional tests for the 'users_api' blueprint.
"""
import datetime as dt

import time_machine
from flask import current_app
from itsdangerous import URLSafeTimedSerializer

from project import mail
from project.models import User
from tests.utilities import check_datetime_values


# -----------------
# User Registration
# -----------------

def test_register_user_valid(test_client):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/users/' page is sent valid user credentials (POST)
    THEN check the response is valid, the user is registered, and an email was queued up to send
    """
    with mail.record_messages() as outbox:
        post_data = {'email': 'patrick@kennedy.com', 'password_plaintext': 'FlaskIsAmazing123'}
        response = test_client.post('/users/', json=post_data)
        assert response.status_code == 201
        assert response.json['email'] == 'patrick@kennedy.com'
        assert response.json['id'] == 1
        assert len(outbox) == 1
        assert outbox[0].subject == 'Flask Journal API - Confirm Your Email Address'
        assert outbox[0].sender == 'flaskjournalapp@gmail.com'
        assert outbox[0].recipients[0] == 'patrick@kennedy.com'
        assert 'http://localhost/users/confirm/' in outbox[0].html


def test_register_user_duplicate(test_client):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/users/' page is sent valid user credentials (POST) twice with the same email address
    THEN check that the 2nd registration attempt is invalid
    """
    post_data = {'email': 'patrick@flaskjournalapi.com', 'password_plaintext': 'FlaskIsAmazing123'}
    response = test_client.post('/users/', json=post_data)
    assert response.status_code == 201

    response = test_client.post('/users/', json=post_data)
    assert response.status_code == 400


def test_register_user_invalid_http_method(test_client):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/users/' page is requested (GET)
    THEN check that a 405 (Method Not Allowed) response is returned
    """
    response = test_client.get('/users/')
    assert response.status_code == 405
    assert response.json['code'] == 405
    assert response.json['name'] == 'Method Not Allowed'


# ------------------------
# Get Authentication Token
# ------------------------

def test_get_authentication_token_valid(test_client, default_user):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/users/get-auth-token' page is sent valid user credentials (POST)
    THEN check the response is valid
    """
    response = test_client.post('/users/get-auth-token', auth=('pkennedy@hey.com', 'FlaskIsTheBest'))
    assert response.status_code == 200
    assert response.json['token'] is not None


def test_get_authentication_token_incorrect_password(test_client, default_user):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/users/get-auth-token' page is sent invalid user credentials (POST)
    THEN check that a 401 (Unauthorized) response is returned
    """
    response = test_client.post('/users/get-auth-token', auth=('pkennedy@hey.com', 'FlaskIsOK'))
    assert response.status_code == 401
    assert response.json['code'] == 401
    assert response.json['message'] == 'Unauthorized'
    assert 'token' not in response.json


def test_get_authentication_token_non_registered_user(test_client, default_user):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/users/get-auth-token' page is sent invalid user credentials (POST)
    THEN check that a 401 (Unauthorized) response is returned
    """
    response = test_client.post('/users/get-auth-token', auth=('patrick@kennedy.org', 'FlaskFlask'))
    assert response.status_code == 401
    assert response.json['code'] == 401
    assert response.json['message'] == 'Unauthorized'
    assert 'token' not in response.json


# ------------
# User Profile
# ------------

@time_machine.travel("2022-05-27 16:37 +0000")
def test_user_profile_valid(test_client, authentication_token):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/users/account' page is sent valid user credentials (GET)
    THEN check the response is valid
    """
    headers = {'Authorization': f'Bearer {authentication_token}'}
    response = test_client.get('/users/account', headers=headers)
    assert response.status_code == 200
    assert len(response.json) == 6
    assert response.json['id'] == 3
    assert response.json['email'] == 'pkennedy@hey.com'
    assert not response.json['email_confirmed']


def test_user_profile_invalid_authentication(test_client):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/users/account' page is requested (GET) with an invalid authentication header
    THEN check that a 401 (Unauthorized) response is returned
    """
    headers = {'Authorization': f'123456ABCD'}
    response = test_client.get('/users/account', headers=headers)
    assert response.status_code == 401
    assert response.json['code'] == 401
    assert response.json['message'] == 'Unauthorized'


def test_user_profile_missing_authentication(test_client):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/users/account' page is requested (GET) with the authentication header missing
    THEN check that a 401 (Unauthorized) response is returned
    """
    response = test_client.get('/users/account')
    assert response.status_code == 401
    assert response.json['code'] == 401
    assert response.json['message'] == 'Unauthorized'


# ---------------
# Change Password
# ---------------

def test_change_password_missing_old_password(test_client, authentication_token):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/users/account' page is sent an invalid (missing entry) password set for updating (PUT)
    THEN check that a 400 (Bad Request) response is returned
    """
    headers = {'Authorization': f'Bearer {authentication_token}'}
    response = test_client.put(
        '/users/account',
        json={'new_password_plaintext': 'FlaskIsAwesome123'},  # Missing `old_password_plaintext` field!!
        headers=headers
    )
    assert response.status_code == 400
    assert response.json['messages']['json']['old_password_plaintext'][0] == 'Missing data for required field.'


def test_change_password_missing_new_password(test_client, authentication_token):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/users/account' page is sent an invalid (missing entry) password set for updating (PUT)
    THEN check that a 400 (Bad Request) response is returned
    """
    headers = {'Authorization': f'Bearer {authentication_token}'}
    response = test_client.put(
        '/users/account',
        json={'old_password_plaintext': 'FlaskIsTheBest'},  # Missing `new_password_plaintext` field!!
        headers=headers
    )
    assert response.status_code == 400
    assert response.json['messages']['json']['new_password_plaintext'][0] == 'Missing data for required field.'


def test_change_password_invalid_authentication(test_client):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/users/account' page is sent a valid password set for updating (PUT) with an invalid authentication header
    THEN check that a 401 (Unauthorized) response is returned
    """
    headers = {'Authorization': f'987654321ABCD'}
    response = test_client.put(
        '/users/account',
        json={
            'old_password_plaintext': 'FlaskIsTheBest',
            'new_password_plaintext': 'FlaskIsAwesome123'
        },
        headers=headers
    )
    assert response.status_code == 401
    assert response.json['code'] == 401
    assert response.json['message'] == 'Unauthorized'


def test_change_password_missing_authentication(test_client):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/users/account' page is sent a valid password set for updating (PUT) with the authentication header missing
    THEN check that a 401 (Unauthorized) response is returned
    """
    response = test_client.put(
        '/users/account',
        json={
            'old_password_plaintext': 'FlaskIsTheBest',
            'new_password_plaintext': 'FlaskIsAwesome123'
        }
    )
    assert response.status_code == 401
    assert response.json['code'] == 401
    assert response.json['message'] == 'Unauthorized'


def test_change_password_incorrect_old_password(test_client, authentication_token):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/users/account' page is sent a password set with an incorrect old password for updating (PUT)
    THEN check that a 400 (Bad Request) response is returned
    """
    headers = {'Authorization': f'Bearer {authentication_token}'}
    response = test_client.put(
        '/users/account',
        json={
            'old_password_plaintext': 'FlaskIsJustOK',
            'new_password_plaintext': 'FlaskIsAwesome123'
        },
        headers=headers
    )
    assert response.status_code == 400
    assert response.json['code'] == 400
    assert response.json['name'] == 'Bad Request'


# ------------------------
# Email Confirmation Token
# ------------------------

def test_confirm_email_valid(test_client):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/users/confirm/<token>' page is requested (GET) with valid data
    THEN check that the user's email address is marked as confirmed
    """
    # Create the unique token for confirming a user's email address
    confirm_serializer = URLSafeTimedSerializer(current_app.config['SECRET_KEY'])
    token = confirm_serializer.dumps('pkennedy@hey.com', salt='email-confirmation-salt')

    response = test_client.get('/users/confirm/'+token, follow_redirects=True)
    assert response.status_code == 200
    assert b'Email was successfully confirmed!' in response.data
    user = User.query.filter_by(email='pkennedy@hey.com').first()
    assert user.email_confirmed


def test_confirm_email_already_confirmed(test_client):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/users/confirm/<token>' page is requested (GET) with valid data
         but the user's email is already confirmed
    THEN check that the user's email address is marked as confirmed
    """
    # Create the unique token for confirming a user's email address
    confirm_serializer = URLSafeTimedSerializer(current_app.config['SECRET_KEY'])
    token = confirm_serializer.dumps('patrick@flaskjournalapi.com', salt='email-confirmation-salt')

    # Confirm the user's email address
    test_client.get('/users/confirm/'+token, follow_redirects=True)

    # Process a valid confirmation link for a user that has their email address already confirmed
    response = test_client.get('/users/confirm/'+token, follow_redirects=True)
    assert response.status_code == 200
    assert b'Email has been previously confirmed.' in response.data
    user = User.query.filter_by(email='patrick@flaskjournalapi.com').first()
    assert user.email_confirmed


def test_confirm_email_invalid(test_client):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/users/confirm/<token>' page is is requested (GET) with invalid data
    THEN check that the link was not accepted
    """
    response = test_client.get('/users/confirm/bad_confirmation_link', follow_redirects=True)
    assert response.status_code == 200
    assert b'Invalid or expired confirmation link received.' in response.data


# -------------------------------
# Re-send Email Confirmation Link
# -------------------------------

@time_machine.travel("2022-05-27 16:37 +0000")
def test_resend_email_confirmation_link_valid(test_client, authentication_token_other_user):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/users/resend_email_confirmation' page is sent valid user credentials (GET)
    THEN check the response is valid and an email was queued up to send
    """
    with mail.record_messages() as outbox:
        headers = {'Authorization': f'Bearer {authentication_token_other_user}'}
        response = test_client.get('/users/resend_email_confirmation', headers=headers)
        assert response.status_code == 200
        assert response.json['email'] == 'patkennedy79@gmail.com'
        assert response.json['id'] == 4
        assert not response.json['email_confirmed']
        assert len(outbox) == 1
        assert outbox[0].subject == 'Flask Journal API - Confirm Your Email Address'
        assert outbox[0].sender == 'flaskjournalapp@gmail.com'
        assert outbox[0].recipients[0] == 'patkennedy79@gmail.com'
        assert 'http://localhost/users/confirm/' in outbox[0].html

    # Check that the confirmation email was sent for the default user
    user = User.query.filter_by(email='patkennedy79@gmail.com').first()
    assert not user.email_confirmed
    check_datetime_values(dt.datetime.utcnow(), user.email_confirmation_sent_on)


def test_resend_email_confirmation_link_already_confirmed(test_client, authentication_token):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/users/resend_email_confirmation' page is sent valid user credentials (GET)
         for a user with the email already confirmed
    THEN check that a 400 (Bad Request) response is returned
    """
    headers = {'Authorization': f'Bearer {authentication_token}'}
    response = test_client.get('/users/resend_email_confirmation', headers=headers)
    assert response.status_code == 400
    assert response.json['code'] == 400
    assert response.json['name'] == 'Bad Request'


def test_resend_email_confirmation_link_invalid_authentication(test_client):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/users/resend_email_confirmation' page is sent a request (GET) with an invalid authentication header
    THEN check that a 401 (Unauthorized) response is returned
    """
    headers = {'Authorization': f'987654321ABCD'}
    response = test_client.get('/users/resend_email_confirmation', headers=headers)
    assert response.status_code == 401
    assert response.json['code'] == 401
    assert response.json['message'] == 'Unauthorized'


def test_resend_email_confirmation_link_missing_authentication(test_client):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/users/resend_email_confirmation' page is requested (GET) with the authentication header missing
    THEN check that a 401 (Unauthorized) response is returned
    """
    response = test_client.get('/users/resend_email_confirmation')
    assert response.status_code == 401
    assert response.json['code'] == 401
    assert response.json['message'] == 'Unauthorized'


# -----------------------------------------------
# Forgot Password (i.e. Send Password Reset Link)
# -----------------------------------------------

def test_forgot_password_valid(test_client, default_user):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/users/forgot-password' page is sent a valid email (PUT) for a user with a confirmed email
    THEN check the response is valid and an email was queued up to send
    """
    with mail.record_messages() as outbox:
        response = test_client.put('/users/forgot-password', json={'email': 'pkennedy@hey.com'})
        assert response.status_code == 200
        assert response.json['message'] == 'Please check your email for a password reset link!'
        assert len(outbox) == 1
        assert outbox[0].subject == 'Flask Journal API - Password Reset Requested'
        assert outbox[0].sender == 'flaskjournalapp@gmail.com'
        assert outbox[0].recipients[0] == 'pkennedy@hey.com'
        assert 'http://localhost/users/password_reset_via_token/' in outbox[0].html


def test_forgot_password_email_not_confirmed(test_client, default_user):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/users/forgot-password' page is sent a valid email (PUT) for a user without a confirmed email
    THEN check that a 400 (Bad Request) response is returned
    """
    response = test_client.put('/users/forgot-password', json={'email': 'patkennedy79@gmail.com'})
    assert response.status_code == 400
    assert response.json['code'] == 400
    assert response.json['name'] == 'Bad Request'
    assert response.json['description'] == 'Password reset link cannot be sent to an unconfirmed email address.'


def test_forgot_password_email_not_in_database(test_client):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/users/forgot-password' page is sent an invalid email (PUT)
    THEN check that a 400 (Bad Request) response is returned
    """
    response = test_client.put('/users/forgot-password', json={'email': 'patkennedy79@yahoo.com'})  # Invalid
    assert response.status_code == 400
    assert response.json['code'] == 400
    assert response.json['name'] == 'Bad Request'


def test_forgot_password_email_field_substituted(test_client):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/users/forgot-password' page is sent an request without an email specified (PUT)
    THEN check that a 400 (Bad Request) response is returned
    """
    response = test_client.put('/users/forgot-password', json={'user': 'patkennedy79@gmail.com'})  # Invalid
    assert response.status_code == 400
    assert response.json['messages']['json']['user'][0] == 'Unknown field.'


def test_forgot_password_email_field_missing(test_client):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/users/forgot-password' page is sent an request without an email specified (PUT)
    THEN check that a 400 (Bad Request) response is returned
    """
    response = test_client.put('/users/forgot-password')  # Invalid
    assert response.status_code == 400
    assert response.json['messages']['json']['email'][0] == 'Missing data for required field.'


# -----------------------------
# Password Reset via Email Link
# -----------------------------

def test_get_password_reset_valid_token(test_client):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/users/password_reset_via_email/<token>' page is requested (GET) with a valid token
    THEN check that the page is successfully returned
    """
    password_reset_serializer = URLSafeTimedSerializer(current_app.config['SECRET_KEY'])
    token = password_reset_serializer.dumps('pkennedy@hey.com', salt='password-reset-salt')

    response = test_client.get('/users/password_reset_via_token/' + token, follow_redirects=True)
    assert response.status_code == 200
    assert b'Password Reset' in response.data
    assert b'New Password' in response.data
    assert b'Submit' in response.data


def test_get_password_reset_invalid_token(test_client):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/users/password_reset_via_email/<token>' page is requested (GET) with an invalid token
    THEN check that an error message is displayed
    """
    token = 'invalid_token'

    response = test_client.get('/users/password_reset_via_token/' + token, follow_redirects=True)
    assert response.status_code == 200
    assert b'Password Reset' not in response.data
    assert b'Invalid or expired password reset link received.' in response.data


def test_post_password_reset_valid_token(test_client):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/users/password_reset_via_email/<token>' page is posted to (POST) with a valid token
    THEN check that the password provided is processed
    """
    password_reset_serializer = URLSafeTimedSerializer(current_app.config['SECRET_KEY'])
    token = password_reset_serializer.dumps('pkennedy@hey.com', salt='password-reset-salt')

    response = test_client.post('/users/password_reset_via_token/' + token,
                                data={'password': 'FlaskIsTheBest987'},
                                follow_redirects=True)
    assert response.status_code == 200
    assert b'Your password was successfully updated!' in response.data


def test_post_password_reset_invalid_token(test_client):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/users/password_reset_via_email/<token>' page is posted to (POST) with an invalid token
    THEN check that the password provided is processed
    """
    token = 'invalid_token'

    response = test_client.post('/users/password_reset_via_token/' + token,
                                data={'password': 'FlaskIsStillGreat45678'},
                                follow_redirects=True)
    assert response.status_code == 200
    assert b'Your password was successfully updated!' not in response.data
    assert b'Invalid or expired password reset link received.' in response.data
