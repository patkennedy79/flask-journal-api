"""
This file (test_journal_api.py) contains the functional tests for the 'journal_api' blueprint.
"""


# -------------------
# Get Journal Entries
# -------------------

def test_get_journal_entries_valid(test_client, default_journal, authentication_token):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/journal/' page is requested (GET)
    THEN check the response is valid (200 OK)
    """
    headers = {'Authorization': f'Bearer {authentication_token}'}
    response = test_client.get('/journal/', headers=headers)
    assert response.status_code == 200
    assert len(response.json) == 3


def test_get_journal_entries_invalid_authentication(test_client, default_journal):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/journal/' page is requested (GET) with an invalid authentication header
    THEN check that a 401 (Unauthorized) response is returned
    """
    headers = {'Authorization': f'123456ABCD'}
    response = test_client.get('/journal/', headers=headers)
    assert response.status_code == 401
    assert response.json['code'] == 401
    assert response.json['message'] == 'Unauthorized'


def test_get_journal_entries_missing_authentication(test_client, default_journal):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/journal/' page is requested (GET) with the authentication header missing
    THEN check that a 401 (Unauthorized) response is returned
    """
    response = test_client.get('/journal/')
    assert response.status_code == 401
    assert response.json['code'] == 401
    assert response.json['message'] == 'Unauthorized'


# -----------------
# Add Journal Entry
# -----------------

def test_add_journal_entry_valid(test_client, default_journal, authentication_token):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/journal/' page is sent a valid journal entry (POST)
    THEN check the response is valid (201 Created)
    """
    headers = {'Authorization': f'Bearer {authentication_token}'}
    response = test_client.post(
        '/journal/',
        json={'entry': 'New Journal Entry'},
        headers=headers
    )
    assert response.status_code == 201
    assert response.json['entry'] == 'New Journal Entry'
    assert response.json['id'] == 4


def test_add_journal_entry_invalid(test_client, default_journal, authentication_token):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/journal/' page is sent an invalid (missing entry) journal entry (POST)
    THEN check that a 400 (Bad Request) response is returned
    """
    headers = {'Authorization': f'Bearer {authentication_token}'}
    response = test_client.post(
        '/journal/',
        json={'author': '3'},  # Missing `entry` field!!
        headers=headers
    )
    assert response.status_code == 400
    assert response.json['messages']['json']['entry'][0] == 'Missing data for required field.'


def test_add_journal_entry_invalid_authentication(test_client, default_journal):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/journal/' page is sent a valid journal entry (POST) with an invalid authentication header
    THEN check that a 401 (Unauthorized) response is returned
    """
    headers = {'Authorization': f'123456ABCD'}
    response = test_client.post(
        '/journal/',
        json={'entry': 'New Journal Entry'},
        headers=headers
    )
    assert response.status_code == 401
    assert response.json['code'] == 401
    assert response.json['message'] == 'Unauthorized'


def test_add_journal_entry_missing_authentication(test_client, default_journal):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/journal/' page is sent a valid journal entry (POST) with the authentication header missing
    THEN check that a 401 (Unauthorized) response is returned
    """
    response = test_client.post('/journal/', json={'entry': 'New Journal Entry'})
    assert response.status_code == 401
    assert response.json['code'] == 401
    assert response.json['message'] == 'Unauthorized'


# -----------------
# Get Journal Entry
# -----------------

def test_get_journal_entry_valid(test_client, default_journal, authentication_token):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/journal/2' page is requested (GET)
    THEN check the response is valid (200 OK)
    """
    headers = {'Authorization': f'Bearer {authentication_token}'}
    response = test_client.get('/journal/2', headers=headers)
    assert response.status_code == 200
    assert response.json['entry'] == 'Journal Entry 2'
    assert response.json['id'] == 2


def test_get_journal_entry_invalid_index(test_client, default_journal, authentication_token):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/journal/10' page is requested (GET)
    THEN check that a 404 (Not Found) response is returned
    """
    headers = {'Authorization': f'Bearer {authentication_token}'}
    response = test_client.get('/journal/10', headers=headers)
    assert response.status_code == 404
    assert response.json['code'] == 404
    assert response.json['name'] == 'Not Found'


def test_get_journal_entry_invalid_authentication(test_client, default_journal):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/journal/3' page is requested (GET) with an invalid authentication header
    THEN check that a 401 (Unauthorized) response is returned
    """
    headers = {'Authorization': f'BAD-AUTH'}
    response = test_client.get('/journal/3', headers=headers)
    assert response.status_code == 401
    assert response.json['code'] == 401
    assert response.json['message'] == 'Unauthorized'


def test_get_journal_entry_missing_authentication(test_client, default_journal):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/journal/3' page is requested (GET) with the authentication header missing
    THEN check that a 401 (Unauthorized) response is returned
    """
    response = test_client.get('/journal/3')
    assert response.status_code == 401
    assert response.json['code'] == 401
    assert response.json['message'] == 'Unauthorized'


def test_get_journal_entry_not_author(test_client, default_journal, authentication_token_other_user):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/journal/2' page is requested (GET) by the user that didn't author the entry
    THEN check that a 403 (Forbidden) response is returned
    """
    headers = {'Authorization': f'Bearer {authentication_token_other_user}'}
    response = test_client.get('/journal/2', headers=headers)
    assert response.status_code == 403
    assert response.json['code'] == 403
    assert response.json['name'] == 'Forbidden'


# --------------------
# Update Journal Entry
# --------------------

def test_update_journal_entry_valid(test_client, default_journal, authentication_token):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/journal/1' page is sent a valid journal entry for updating (PUT)
    THEN check the response is valid (200 OK)
    """
    headers = {'Authorization': f'Bearer {authentication_token}'}
    response = test_client.put(
        '/journal/1',
        json={'entry': 'Updated journal entry!'},
        headers=headers
    )
    assert response.status_code == 200
    assert response.json['entry'] == 'Updated journal entry!'
    assert response.json['id'] == 1


def test_update_journal_entry_missing_entry(test_client, default_journal, authentication_token):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/journal/1' page is sent an invalid (missing entry) journal entry for updating (PUT)
    THEN check that a 400 (Bad Request) response is returned
    """
    headers = {'Authorization': f'Bearer {authentication_token}'}
    response = test_client.put(
        '/journal/1',
        json={'author': '4'},  # Missing `entry` field!!
        headers=headers
    )
    assert response.status_code == 400
    assert response.json['messages']['json']['entry'][0] == 'Missing data for required field.'


def test_update_journal_entry_invalid_index(test_client, default_journal, authentication_token):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/journal/17' page is sent a valid journal entry for updating (PUT)
    THEN check that a 404 (Not Found) response is returned
    """
    headers = {'Authorization': f'Bearer {authentication_token}'}
    response = test_client.put(
        '/journal/17',
        json={'entry': 'Updated journal entry!'},
        headers=headers
    )
    assert response.status_code == 404
    assert response.json['code'] == 404
    assert response.json['name'] == 'Not Found'


def test_update_journal_entry_invalid_authentication(test_client, default_journal):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/journal/1' page is sent a valid journal entry for updating (PUT) with an invalid authentication header
    THEN check that a 401 (Unauthorized) response is returned
    """
    headers = {'Authorization': f'987654321ABCD'}
    response = test_client.put(
        '/journal/1',
        json={'entry': 'Updated journal entry!'},
        headers=headers
    )
    assert response.status_code == 401
    assert response.json['code'] == 401
    assert response.json['message'] == 'Unauthorized'


def test_update_journal_entry_missing_authentication(test_client, default_journal):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/journal/1' page is sent a valid journal entry for updating (PUT) with the authentication header missing
    THEN check that a 401 (Unauthorized) response is returned
    """
    response = test_client.put('/journal/1', json={'entry': 'Updated journal entry!'})
    assert response.status_code == 401
    assert response.json['code'] == 401
    assert response.json['message'] == 'Unauthorized'


def test_update_journal_entry_not_author(test_client, default_journal, authentication_token_other_user):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/journal/3' page is sent a valid journal entry for updating (PUT) by the user that didn't author the entry
    THEN check that a 403 (Forbidden) response is returned
    """
    headers = {'Authorization': f'Bearer {authentication_token_other_user}'}
    response = test_client.put(
        '/journal/3',
        json={'entry': 'Updated journal entry!'},
        headers=headers
    )
    assert response.status_code == 403
    assert response.json['code'] == 403
    assert response.json['name'] == 'Forbidden'


# --------------------
# Delete Journal Entry
# --------------------

def test_delete_journal_entry_valid(test_client, default_journal, authentication_token):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/journal/2' page is sent a delete request (DELETE)
    THEN check the response is valid (204 No Content)
    """
    headers = {'Authorization': f'Bearer {authentication_token}'}
    response = test_client.delete('/journal/2', headers=headers)
    assert response.status_code == 204


def test_delete_journal_entry_invalid_index(test_client, default_journal, authentication_token):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/journal/12' page is sent a delete request (DELETE)
    THEN check that a 404 (Not Found) response is returned
    """
    headers = {'Authorization': f'Bearer {authentication_token}'}
    response = test_client.delete('/journal/12', headers=headers)
    assert response.status_code == 404
    assert response.json['code'] == 404
    assert response.json['name'] == 'Not Found'


def test_delete_journal_entry_invalid_authentication(test_client, default_journal):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/journal/1' page is sent a delete request (DELETE) with an invalid authentication header
    THEN check that a 401 (Unauthorized) response is returned
    """
    headers = {'Authorization': f'BADBADBAD'}
    response = test_client.delete('/journal/1', headers=headers)
    assert response.status_code == 401
    assert response.json['code'] == 401
    assert response.json['message'] == 'Unauthorized'


def test_delete_journal_entry_missing_authentication(test_client, default_journal):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/journal/1' page is sent a delete request (DELETE) with the authentication header missing
    THEN check that a 401 (Unauthorized) response is returned
    """
    response = test_client.delete('/journal/1')
    assert response.status_code == 401
    assert response.json['code'] == 401
    assert response.json['message'] == 'Unauthorized'


def test_delete_journal_entry_not_author(test_client, default_journal, authentication_token_other_user):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/journal/3' page is sent a delete request (DELETE) by the user that didn't author the entry
    THEN check that a 403 (Forbidden) response is returned
    """
    headers = {'Authorization': f'Bearer {authentication_token_other_user}'}
    response = test_client.delete('/journal/3', headers=headers)
    assert response.status_code == 403
    assert response.json['code'] == 403
    assert response.json['name'] == 'Forbidden'
