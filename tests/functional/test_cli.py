"""
This file (test_cli.py) contains the functional tests for the CLI (Command-Line Interface) functions.
"""
from project import database
from project.models import Entry


def test_initialize_database(cli_test_client):
    """
    GIVEN a Flask application configured for testing
    WHEN the 'flask init_db' command is called from the command line
    THEN check the response is valid
    """
    output = cli_test_client.invoke(args=['init_db'])
    assert output.exit_code == 0
    assert 'Initializing the SQLite database!' in output.output


def test_fill_database(cli_test_client):
    """
    GIVEN a Flask application configured for testing
    WHEN the 'flask fill_db' command is called from the command line
    THEN check the response is valid
    """
    output = cli_test_client.invoke(args=['fill_db'])
    assert output.exit_code == 0
    assert 'Filled the SQLite database with 2 users and 3 entries!' in output.output


def test_create_admin(cli_test_client):
    """
    GIVEN a Flask application configured for testing
    WHEN the 'flask admin create_admin_user' command is called from the command line
    THEN check the response is valid
    """
    output = cli_test_client.invoke(args=['admin', 'create_admin_user', 'patrickkennedy@email.com', 'FlaskRules'])
    assert output.exit_code == 0
    assert 'Created new admin user (patrickkennedy@email.com)!' in output.output
