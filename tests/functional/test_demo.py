"""
This file (test_demo.py) contains the functional tests for the 'demo_api' blueprint.
"""


# --------------------------
# Get Journal Entries (Demo)
# --------------------------

def test_get_demo_journal_valid(test_client_without_database):
    """
    GIVEN a Flask application configured for testing (without a database)
    WHEN the '/demo/journal/' page is requested (GET)
    THEN check the response is valid (200 OK)
    """
    response = test_client_without_database.get('/demo/journal/')
    assert response.status_code == 200
    assert len(response.json) == 5
    assert response.json[0]['id'] == 1
    assert response.json[0]['entry'] == 'I went for a great walk at the park today.'
    assert response.json[1]['id'] == 2
    assert response.json[1]['entry'] == 'I tried a new pasta recipe for dinner tonight.'
    assert response.json[2]['id'] == 3
    assert response.json[2]['entry'] == 'There was a great new movie on Netflix that I watched tonight.'
    assert response.json[3]['id'] == 4
    assert response.json[3]['entry'] == 'There was so much fresh fruit at the grocery store, so I made a great fruit salad with dinner.'
    assert response.json[4]['id'] == 5
    assert response.json[4]['entry'] == 'I got an email from an old friend today that was a really nice surprise.'


# --------------------------
# Add Journal Entries (Demo)
# --------------------------

def test_demo_add_journal_entry_valid(test_client_without_database):
    """
    GIVEN a Flask application configured for testing (without a database)
    WHEN the '/demo/journal/' page is sent a valid journal entry (POST)
    THEN check the response is valid (200 OK)
    """
    response = test_client_without_database.post('/demo/journal/', json={'entry': 'New Journal Entry'})
    assert response.status_code == 201
    assert len(response.json) == 5
    assert response.json['id'] == 6
    assert response.json['entry'] == 'New Journal Entry'
    assert response.json['user_id'] == 0


def test_demo_add_journal_entry_invalid(test_client_without_database):
    """
    GIVEN a Flask application configured for testing (without a database)
    WHEN the '/demo/journal/' page is sent an invalid (missing entry) journal entry (POST)
    THEN check that a 400 (Bad Request) response is returned
    """
    response = test_client_without_database.post(
        '/demo/journal/',
        json={'author': '3'}  # Missing `entry` field!!
    )
    assert response.status_code == 400
    assert response.json['messages']['json']['entry'][0] == 'Missing data for required field.'


# ------------------------
# Get Journal Entry (Demo)
# ------------------------

def test_demo_get_journal_entry_valid(test_client_without_database):
    """
    GIVEN a Flask application configured for testing (without a database)
    WHEN the '/demo/journal/2' page is requested (GET)
    THEN check the response is valid (200 OK)
    """
    response = test_client_without_database.get('/demo/journal/2')
    assert response.status_code == 200
    assert response.json['id'] == 2
    assert response.json['entry'] == 'I tried a new pasta recipe for dinner tonight.'


def test_demo_get_journal_entry_invalid_index_1(test_client_without_database):
    """
    GIVEN a Flask application configured for testing (without a database)
    WHEN the '/demo/journal/0' page is requested (GET)
    THEN check that a 404 (Not Found) response is returned
    """
    response = test_client_without_database.get('/demo/journal/0')
    assert response.status_code == 404
    assert response.json['code'] == 404
    assert response.json['name'] == 'Not Found'


def test_demo_get_journal_entry_invalid_index_2(test_client_without_database):
    """
    GIVEN a Flask application configured for testing (without a database)
    WHEN the '/demo/journal/6' page is requested (GET)
    THEN check that a 404 (Not Found) response is returned
    """
    response = test_client_without_database.get('/demo/journal/6')
    assert response.status_code == 404
    assert response.json['code'] == 404
    assert response.json['name'] == 'Not Found'


# ---------------------------
# Update Journal Entry (Demo)
# ---------------------------

def test_demo_update_journal_entry_valid(test_client_without_database):
    """
    GIVEN a Flask application configured for testing (without a database)
    WHEN the '/demo/journal/1' page is sent a valid journal entry for updating (PUT)
    THEN check the response is valid (200 OK)
    """
    response = test_client_without_database.put(
        '/demo/journal/1',
        json={'entry': 'Updated journal entry!'}
    )
    assert response.status_code == 200
    assert response.json['entry'] == 'Updated journal entry!'
    assert response.json['id'] == 1


def test_demo_update_journal_entry_missing_entry(test_client_without_database):
    """
    GIVEN a Flask application configured for testing (without a database)
    WHEN the '/demo/journal/1' page is sent an invalid (missing entry) journal entry for updating (PUT)
    THEN check that a 400 (Bad Request) response is returned
    """
    response = test_client_without_database.put(
        '/demo/journal/1',
        json={'author': '4'}  # Missing `entry` field!!
    )
    assert response.status_code == 400
    assert response.json['messages']['json']['entry'][0] == 'Missing data for required field.'


def test_demo_update_journal_entry_invalid_index_1(test_client_without_database):
    """
    GIVEN a Flask application configured for testing (without a database)
    WHEN the '/demo/journal/0' page is sent a valid journal entry for updating (PUT)
    THEN check that a 404 (Not Found) response is returned
    """
    response = test_client_without_database.put(
        '/demo/journal/0',
        json={'entry': 'Updated journal entry!'}
    )
    assert response.status_code == 404
    assert response.json['code'] == 404
    assert response.json['name'] == 'Not Found'


def test_demo_update_journal_entry_invalid_index_2(test_client_without_database):
    """
    GIVEN a Flask application configured for testing (without a database)
    WHEN the '/demo/journal/6' page is sent a valid journal entry for updating (PUT)
    THEN check that a 404 (Not Found) response is returned
    """
    response = test_client_without_database.put(
        '/demo/journal/6',
        json={'entry': 'Updated journal entry!'}
    )
    assert response.status_code == 404
    assert response.json['code'] == 404
    assert response.json['name'] == 'Not Found'


# --------------------
# Delete Journal Entry
# --------------------

def test_demo_delete_journal_entry_valid(test_client_without_database):
    """
    GIVEN a Flask application configured for testing (without a database)
    WHEN the '/demo/journal/5' page is sent a delete request (DELETE)
    THEN check the response is valid (204 No Content)
    """
    response = test_client_without_database.delete('/demo/journal/5')
    assert response.status_code == 204


def test_demo_delete_journal_entry_invalid_index_1(test_client_without_database):
    """
    GIVEN a Flask application configured for testing (without a database)
    WHEN the '/demo/journal/0' page is sent a delete request (DELETE)
    THEN check that a 404 (Not Found) response is returned
    """
    response = test_client_without_database.delete('/demo/journal/0')
    assert response.status_code == 404
    assert response.json['code'] == 404
    assert response.json['name'] == 'Not Found'


def test_demo_delete_journal_entry_invalid_index_2(test_client_without_database):
    """
    GIVEN a Flask application configured for testing (without a database)
    WHEN the '/demo/journal/6' page is sent a delete request (DELETE)
    THEN check that a 404 (Not Found) response is returned
    """
    response = test_client_without_database.delete('/demo/journal/6')
    assert response.status_code == 404
    assert response.json['code'] == 404
    assert response.json['name'] == 'Not Found'
