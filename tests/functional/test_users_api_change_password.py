"""
This file (test_users_api_change_password.py) contains the functional test
for changing a user's password in the 'users_api' blueprint.

This single test is in a separate module (i.e. file), as it causes
the authentication token for the default user to be revoked, which can
adversely affect other tests in the same module.
"""


def test_change_password_valid(test_client, authentication_token):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/users/account' page is sent a valid password set for updating (PUT)
    THEN check the response is valid and the authentication token was revoked
    """
    headers = {'Authorization': f'Bearer {authentication_token}'}
    response = test_client.put(
        '/users/account',
        json={
            'old_password_plaintext': 'FlaskIsTheBest',
            'new_password_plaintext': 'FlaskIsAwesome123'
        },
        headers=headers)
    assert response.status_code == 200
    assert len(response.json) == 6
    assert response.json['email'] == 'pkennedy@hey.com'
    assert response.json['id'] == 1
    assert not response.json['email_confirmed']

    # Check that the original authentication token no longer works (unauthorized)
    response = test_client.get('/journal/', headers=headers)
    assert response.status_code == 401
    assert response.json['code'] == 401
    assert response.json['message'] == 'Unauthorized'

    # Check that retrieving the authentication token with the old password is unsuccessful
    response = test_client.post('/users/get-auth-token', auth=('pkennedy@hey.com', 'FlaskIsTheBest'))
    assert response.status_code == 401
    assert response.json['code'] == 401
    assert response.json['message'] == 'Unauthorized'
    assert 'token' not in response.json

    # Check that retrieving the authentication token with the new password is successful
    response = test_client.post('/users/get-auth-token', auth=('pkennedy@hey.com', 'FlaskIsAwesome123'))
    assert response.status_code == 200
    assert response.json['token'] is not None
