"""
This file (test_models.py) contains the unit tests for the models.py file.
"""
import datetime
import datetime as dt

import time_machine

from project.models import Entry, User
from tests.utilities import check_datetime_values


@time_machine.travel("2022-04-20 08:34 +0000")
def test_new_entry():
    """
    GIVEN an Entry model
    WHEN a new Entry object is created
    THEN check that the entry is defined correctly
    """
    new_entry = Entry('Journal Entry 1', 1)
    assert new_entry.entry == 'Journal Entry 1'
    assert new_entry.user_id == 1
    assert repr(new_entry) == '<Entry: Journal Entry 1>'
    check_datetime_values(dt.datetime.utcnow(), new_entry.created_on)
    check_datetime_values(dt.datetime.utcnow(), new_entry.last_edited_on)


@time_machine.travel("2022-05-21 15:34 +0000")
def test_update_entry():
    """
    GIVEN an Entry model
    WHEN an Entry object is updated
    THEN check that the entry is updated correctly
    """
    new_entry = Entry('Journal Entry 1', 1)
    new_entry.update('Updated Journal Entry 1')
    assert new_entry.entry == 'Updated Journal Entry 1'
    check_datetime_values(dt.datetime.utcnow(), new_entry.last_edited_on)


@time_machine.travel("2022-04-20 08:34 +0000")
def test_new_user():
    """
    GIVEN a User model
    WHEN a new User object is created
    THEN check that the user is defined correctly
    """
    new_user = User('patrick@email.com', 'FlaskIsAwesome1')
    assert new_user.email == 'patrick@email.com'
    assert new_user.password_hashed != 'FlaskIsAwesome1'
    assert new_user.is_password_correct('FlaskIsAwesome1')
    assert not new_user.is_password_correct('FlaskIsAwesome2')
    assert repr(new_user) == '<User: patrick@email.com>'
    check_datetime_values(dt.datetime.utcnow(), new_user.registered_on)
    check_datetime_values(dt.datetime.utcnow(), new_user.email_confirmation_sent_on)
    assert not new_user.email_confirmed
    assert new_user.email_confirmed_on is None
    assert not new_user.is_admin()
    assert new_user.get_roles() == ['User']


def test_update_user_password():
    """
    GIVEN a User model
    WHEN the password for a User object is updated
    THEN check that the password is changed correctly
    """
    new_user = User('patrick@email.com', 'FlaskIsAwesome1')
    assert new_user.is_password_correct('FlaskIsAwesome1')
    new_user.set_password('FlaskReallyIsAwesome')
    assert new_user.is_password_correct('FlaskReallyIsAwesome')
    assert not new_user.is_password_correct('FlaskIsAwesome1')


@time_machine.travel("2022-05-24 16:47 +0000")
def test_user_confirm_email_address():
    """
    GIVEN a User model
    WHEN the email address is confirmed for a User object
    THEN check that the email address is confirmed
    """
    new_user = User('patrick@email.com', 'FlaskIsAwesome1')
    check_datetime_values(dt.datetime.utcnow(), new_user.email_confirmation_sent_on)
    assert not new_user.email_confirmed
    assert new_user.email_confirmed_on is None

    new_user.confirm_email_address()

    check_datetime_values(dt.datetime.utcnow(), new_user.email_confirmation_sent_on)
    assert new_user.email_confirmed
    check_datetime_values(dt.datetime.utcnow(), new_user.email_confirmed_on)


@time_machine.travel("2022-05-24 16:47 +0000")
def test_user_revoke_email_address_confirmation():
    """
    GIVEN a User model
    WHEN the email address is revoked for a User object
    THEN check that the email address is no longer confirmed
    """
    new_user = User('patrick@email.com', 'FlaskIsAwesome1')
    assert not new_user.email_confirmed

    new_user.confirm_email_address()
    assert new_user.email_confirmed

    new_user.revoke_email_address_confirmation()
    check_datetime_values(dt.datetime.utcnow(), new_user.email_confirmation_sent_on)
    assert not new_user.email_confirmed
    assert new_user.email_confirmed_on is None


@time_machine.travel("2022-04-19 02:37 +0000")
def test_generate_authentication_token():
    """
    GIVEN a User model
    WHEN the authentication token for a User object is created
    THEN check that the token expiration date is created correctly for 60 minutes in the future
    """
    new_user = User('patrick@email.com', 'FlaskIsAwesome1')
    new_user.generate_auth_token()
    check_datetime_values(dt.datetime.utcnow() + dt.timedelta(minutes=60), new_user.auth_token_expiration)


@time_machine.travel("2022-04-20 08:34 +0000")
def test_revoke_authentication_token():
    """
    GIVEN a User model
    WHEN the authentication token for a User object is revoked
    THEN check that the token expiration date is updated correctly to the current time
    """
    new_user = User('patrick@email.com', 'FlaskIsAwesome1')
    new_user.generate_auth_token()
    new_user.revoke_auth_token()
    check_datetime_values(dt.datetime.utcnow(), new_user.auth_token_expiration)


def test_resend_email_confirmation_link():
    """
    GIVEN a User model
    WHEN the email confirmation link is re-sent
    THEN check that the email confirmation sent field is updated correctly to the current date/time
    """
    with time_machine.travel("2022-04-20 08:34 +0000", tick=False) as traveller:
        new_user = User('patrick@email.com', 'FlaskIsAwesome1')
        check_datetime_values(dt.datetime.utcnow(), new_user.email_confirmation_sent_on)

        # Advance into the future and re-send the email confirmation link
        traveller.shift(dt.timedelta(days=10, hours=3, minutes=17))
        new_user.confirmation_email_link_sent()
        check_datetime_values(dt.datetime.utcnow(), new_user.email_confirmation_sent_on)


def test_new_admin_user():
    """
    GIVEN a User model
    WHEN a new User object is created with admin privileges
    THEN check the email is valid, the hashed password is valid, and the type is Admin
    """
    new_admin_user = User('patrick_admin@email.com', 'FlaskIsTheBest987', 'Admin')
    assert new_admin_user.email == 'patrick_admin@email.com'
    assert new_admin_user.password_hashed != 'FlaskIsTheBest987'
    assert new_admin_user.is_password_correct('FlaskIsTheBest987')
    assert new_admin_user.user_type == 'Admin'
    assert new_admin_user.is_admin()
    assert new_admin_user.get_roles() == ['User', 'Admin']
