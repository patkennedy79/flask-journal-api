import os

import pytest

from project import create_app, database
from project.models import User


# --------
# Fixtures
# --------

@pytest.fixture(scope='module')
def test_client():
    # Create a Flask app configured for testing
    flask_app = create_app()
    flask_app.config.from_object('config.TestingConfig')
    flask_app.extensions['mail'].suppress = True

    # Create a test client using the Flask application configured for testing
    with flask_app.test_client() as testing_client:
        # Establish an application context before accessing the logger and database
        with flask_app.app_context():
            # Initialize the database and the database table(s)
            database.drop_all()
            database.create_all()

        yield testing_client  # this is where the testing happens!

        with flask_app.app_context():
            # Drop the database and the database table(s)
            database.drop_all()


@pytest.fixture(scope='module')
def test_client_without_database():
    # Create a Flask app configured for testing
    flask_app = create_app()
    flask_app.config.from_object('config.TestingConfig')
    flask_app.extensions['mail'].suppress = True

    # Create a test client using the Flask application configured for testing
    with flask_app.test_client() as testing_client:
        yield testing_client  # this is where the testing happens!


@pytest.fixture(scope='module')
def default_user(test_client):
    # Add two users
    test_client.post('/users/', json={'email': 'pkennedy@hey.com', 'password_plaintext': 'FlaskIsTheBest'})
    test_client.post('/users/', json={'email': 'patkennedy79@gmail.com', 'password_plaintext': 'FlaskIsAmazing'})
    return


@pytest.fixture(scope='module')
def authentication_token(test_client, default_user):
    response = test_client.post('/users/get-auth-token', auth=('pkennedy@hey.com', 'FlaskIsTheBest'))
    return response.json['token']


@pytest.fixture(scope='module')
def default_journal(test_client, authentication_token):
    # Add three journal entries
    headers = {'Authorization': f'Bearer {authentication_token}'}
    test_client.post('/journal/', json={'entry': 'Journal Entry 1'}, headers=headers)
    test_client.post('/journal/', json={'entry': 'Journal Entry 2'}, headers=headers)
    test_client.post('/journal/', json={'entry': 'Journal Entry 3'}, headers=headers)
    return


@pytest.fixture(scope='module')
def authentication_token_other_user(test_client, default_user):
    response = test_client.post('/users/get-auth-token', auth=('patkennedy79@gmail.com', 'FlaskIsAmazing'))
    return response.json['token']


@pytest.fixture(scope='module')
def cli_test_client():
    flask_app = create_app()

    # Configure the Flask app for testing
    flask_app.config['TESTING'] = True
    flask_app.config['SQLALCHEMY_DATABASE_URI'] = f"sqlite:///{os.path.join(os.getcwd(), 'instance', 'test.db')}"

    runner = flask_app.test_cli_runner()

    yield runner  # this is where the testing happens!


@pytest.fixture(scope='module')
def admin_user(test_client):
    new_admin_user = User('patrick_admin@email.com', 'FlaskIsTheBest987', 'Admin')
    database.session.add(new_admin_user)
    database.session.commit()
    return


@pytest.fixture(scope='function')
def admin_user_login(test_client, admin_user):
    # Log in the admin user
    test_client.post('/admin/login',
                     data={'email': 'patrick_admin@email.com', 'password': 'FlaskIsTheBest987'},
                     follow_redirects=True)

    yield   # this is where the testing happens!

    # Log out the default user
    test_client.get('/admin/logout', follow_redirects=True)
