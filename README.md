## Overview

This Flask application provides an API for adding/editing/deleting/viewing journal entries, such as for a gratitude journal or a daily journal.

This project is developed as part of the following blog post on [testdriven.io](https://testdriven.io/):

* [Building a Flask API with APIFairy](https://testdriven.io/blog/flask-apifairy/)

![Building a Flask API with APIFairy](project/static/img/flask_apifairy_social_card.png?raw=true "Building a Flask API with APIFairy")

## Installation Instructions

### Installation

Pull down the source code from this GitLab repository:

```sh
git clone git@gitlab.com:patkennedy79/flask-journal_api.git
```

Create a new virtual environment:

```sh
$ cd flask-journal_api
$ python3 -m venv venv
```

Activate the virtual environment:

```sh
$ source venv/bin/activate
```

Install the python packages specified in requirements.txt:

```sh
(venv) $ pip install -r requirements.txt
```

### Database Initialization

This Flask application needs a SQLite database to store data.  The database should be initialized via the Flask shell:

```
(venv) $ flask shell
>>> from project import database
>>> database.drop_all()
>>> database.create_all()
>>> quit()
(venv) $
```

### Running the Flask Application

Set the file that contains the Flask application and specify that the development environment should be used:

```sh
(venv) $ export FLASK_APP=app.py
(venv) $ export FLASK_ENV=development
```

Run development server to serve the Flask application:

```sh
(venv) $ flask run
```

Navigate to 'http://127.0.0.1:5000/' in your favorite web browser to view the website!

## Configuration

The following environment variables are recommended to be defined:

* SECRET_KEY - see description below
* CONFIG_TYPE - `config.DevelopmentConfig`, `config.ProductionConfig`, or `config.TestConfig`
* DATABASE_URL - URL for the database (either SQLite or Postgres)
* MAIL_USERNAME - username for the email account used for sending out emails from the app
* MAIL_PASSWORD - password for email account

### Secret Key

The 'SECRET_KEY' can be generated using the following commands (assumes Python 3.6 or later):

```sh
(venv) $ python

>>> import secrets
>>> print(secrets.token_bytes(32))
>>> quit()

(venv) $ export SECRET_KEY=<secret_key_generated_in_interpreter>
```

NOTE: If working on Windows, use `set` instead of `export`.

## Key Python Modules Used

* **Flask**: micro-framework for web application development which includes the following dependencies:
  * click: package for creating command-line interfaces (CLI)
  * itsdangerous: cryptographically sign data 
  * Jinja2: templating engine
  * MarkupSafe: escapes characters so text is safe to use in HTML and XML
  * Werkzeug: set of utilities for creating a Python application that can talk to a WSGI server
* **APIFairy**: API framework for Flask which includes the following dependencies:
  * **Flask-Marshmallow** - Flask extension for using Marshmallow (object serialization/deserialization library)
  * **Flask-HTTPAuth** - Flask extension for HTTP authentication
  * **apispec** - API specification generator that supports the OpenAPI specification
* **pytest**: framework for testing Python projects
* **flake8**: static analysis tool
* **pytest-cov**: pytest extension for running coverage.py to check code coverage of tests
* **Flask-SQLAlchemy**: ORM (Object Relational Mapper) for Flask
* **Flask-Migrate**: relational database migration tool for Flask based on alembic
* **Flask-Mail**: Flask extension for sending email
* **requests**: Python library for HTTP

This application is written using Python 3.10.1.

## Testing

To run all the tests:

```sh
(venv) $ python -m pytest -v
```

To check the code coverage of the tests:

```sh
(venv) $ python -m pytest --cov-report term-missing --cov=project
```

NOTE: If working on Windows, use `set` instead of `export`.
